package oodp.assignment2.quiz;

public class QuizData {

	private String name;
	private Question[] questions;

	public QuizData(String name, Question[] questions) {
		this.name = name;
		this.questions = questions;
	}

	public String getName() {
		return this.name;
	}

	public Question getQuestion(int index) {
		return this.questions[index];
	}

	public int getQuestionCount() {
		return this.questions.length;
	}

}

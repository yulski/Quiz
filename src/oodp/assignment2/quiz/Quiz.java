package oodp.assignment2.quiz;

import java.util.Iterator;

public class Quiz implements Iterable<Question> {

	private QuizData data;

	public Quiz(QuizBuilder builder) {
		this.data = builder.getQuizData();
	}

	public String getName() {
		return this.data.getName();
	}

	@Override
	public Iterator<Question> iterator() {
		return new QuizIterator(this.data);
	}

}

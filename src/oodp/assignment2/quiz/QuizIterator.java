package oodp.assignment2.quiz;

import java.util.*;

public class QuizIterator implements Iterator<Question> {

	private int index;
	private QuizData data;

	public QuizIterator(QuizData data) {
		this.index = 0;
		this.data = data;
	}

	@Override
	public boolean hasNext() {
		return this.index < this.data.getQuestionCount();
	}

	@Override
	public Question next() {
		Question question = this.data.getQuestion(this.index);
		this.index++;
		return question;
	}

}

package oodp.assignment2.quiz;

public class Question {

	private String text;
	private String[] options;
	private int correctIndex;
	private transient int answerIndex;

	public Question(String text, String[] options, int correctIndex) {
		this.text = text;
		this.options = options;
		this.correctIndex = correctIndex;
		this.answerIndex = -1;
	}

	public String getText() {
		return this.text;
	}

	public String[] getOptions() {
		return this.options;
	}

	public void answer(int answerIndex) {
		this.answerIndex = answerIndex;
	}

	public boolean isAnswered() {
		return this.answerIndex > -1;
	}

	public boolean isCorrect() {
		return this.answerIndex == this.correctIndex;
	}

	public int getAnswerIndex() {
		return this.answerIndex;
	}

}

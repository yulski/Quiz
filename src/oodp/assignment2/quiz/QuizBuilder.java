package oodp.assignment2.quiz;

import java.util.ArrayList;
import java.util.List;

public class QuizBuilder {

	private String name;
	private List<Question> questions;

	public QuizBuilder() {
		this.questions = new ArrayList<>();
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addQuestion(String text, String[] options, int correctIndex) {
		this.questions.add(new Question(text, options, correctIndex));
	}

	public void removeQuestion(int index) {
		this.questions.remove(index);
	}

	public QuizData getQuizData() {
		Question[] questionArr = this.questions.toArray(new Question[this.questions.size()]);
		return new QuizData(this.name, questionArr);
	}

	public List<Question> getQuestions() {
		return this.questions;
	}

	public Quiz build() {
		return new Quiz(this);
	}

}

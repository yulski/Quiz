package oodp.assignment2.quiz;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import oodp.assignment2.quiz.io.QuizDataMapper;

public class QuizApplication extends Application {

	private static final String TITLE = "Quiz Application";
	private static final int WINDOW_WIDTH = 1150;
	private static final int WINDOW_HEIGHT = 700;
	private static final String WELCOME_IMAGE_LOCATION = "./images/welcome.png";
	private static final int WELCOME_IMAGE_WIDTH = 500;
	private static final int WELCOME_IMAGE_HEIGHT = 350;
	private static final Font TITLE_FONT = new Font(40);

	private static final String CORRECT_ANSWER_INDICATOR = " (correct)";

	private Stage primaryStage;

	private Scene welcomeScene;
	private Scene makeQuizScene;
	private Scene takeQuizScene;
	private Scene quizScene;
	private Scene resultScene;

	private Button makeQuizButton;
	private Button takeQuizButton;
	private Button newQuestionButton;
	private TextField quizNameTextField;
	private Button setQuizNameButton;
	private Button addQuestionTextButton;
	private TextArea addQuestionTextArea;
	private TextArea addAnswerTextArea;
	private CheckBox correctAnswerCheckBox;
	private Button addAnswerButton;
	private TextArea currentQuestionTextArea;
	private ListView<Label> currentAnswersList;
	private Button addCurrentQuestionButton;
	private TextField currentQuizNameTextField;
	private ListView<Label> questionsList;
	private Button submitQuizButton;
	private Button makeQuizBackButton;

	private ListView<Label> selectQuizList;
	private Button startQuizButton;
	private Button takeQuizBackButton;

	private Button finishQuizButton;
	private Button quizBackButton;

	private Button resultBackButton;

	private QuizBuilder builder;

	private Quiz[] quizzes;

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;

		/*
		 * Configuring the welcome scene
		 */
		Text welcomeText = new Text("Welcome");
		welcomeText.setFont(TITLE_FONT);
		welcomeText.setTextAlignment(TextAlignment.CENTER);

		HBox welcomeButtonBox = new HBox(5);
		welcomeButtonBox.setAlignment(Pos.CENTER);
		welcomeButtonBox.setPadding(new Insets(5, 5, 5, 5));
		this.makeQuizButton = new Button("Make a Quiz");
		this.takeQuizButton = new Button("Take a Quiz");

		ObservableList<Node> welcomeButtonPaneChildren = welcomeButtonBox.getChildren();
		welcomeButtonPaneChildren.add(this.makeQuizButton);
		welcomeButtonPaneChildren.add(this.takeQuizButton);

		Image welcomeImage = new Image(new FileInputStream(WELCOME_IMAGE_LOCATION),
				WELCOME_IMAGE_WIDTH, WELCOME_IMAGE_HEIGHT, true, true);
		ImageView welcomeImageView = new ImageView(welcomeImage);

		VBox welcomeBox = new VBox();
		welcomeBox.setAlignment(Pos.CENTER);
		welcomeBox.setStyle("-fx-background-color: #fff");
		ObservableList<Node> welcomePaneChildren = welcomeBox.getChildren();
		welcomePaneChildren.add(welcomeText);
		welcomePaneChildren.add(welcomeButtonBox);
		welcomePaneChildren.add(welcomeImageView);

		this.welcomeScene = new Scene(welcomeBox);

		/*
		 * Configuring the quiz making scene
		 */
		Text makeQuizTitleText = new Text("Make Your Own Quiz");
		makeQuizTitleText.setFont(TITLE_FONT);
		makeQuizTitleText.setTextAlignment(TextAlignment.CENTER);

		Text setQuizNameText = new Text("Enter quiz name");
		this.quizNameTextField = new TextField();
		this.quizNameTextField.setPromptText("Enter the quiz name...");
		this.setQuizNameButton = new Button("Go");

		VBox makeQuizTopBox = new VBox(5);
		ObservableList<Node> makeQuizTopBoxChildren = makeQuizTopBox.getChildren();
		makeQuizTopBoxChildren.add(makeQuizTitleText);
		makeQuizTopBoxChildren.add(setQuizNameText);
		makeQuizTopBoxChildren.add(this.quizNameTextField);
		makeQuizTopBoxChildren.add(this.setQuizNameButton);

		Text addQuestionText = new Text("Add question");
		this.newQuestionButton = new Button("New Question");
		this.addQuestionTextArea = new TextArea();
		this.addQuestionTextArea.setPromptText("Write the question text here...");
		this.addQuestionTextButton = new Button("Go");
		this.addAnswerTextArea = new TextArea();
		this.addAnswerTextArea.setPromptText("Write an answer here...");
		this.correctAnswerCheckBox = new CheckBox("Is correct answer");
		this.addAnswerButton = new Button("Go");

		VBox addQuestionBox = new VBox(5);
		ObservableList<Node> addAnswerBoxChildren = addQuestionBox.getChildren();
		addAnswerBoxChildren.add(addQuestionText);
		addAnswerBoxChildren.add(this.newQuestionButton);
		addAnswerBoxChildren.add(this.addQuestionTextArea);
		addAnswerBoxChildren.add(this.addQuestionTextButton);
		addAnswerBoxChildren.add(this.addAnswerTextArea);
		addAnswerBoxChildren.add(this.correctAnswerCheckBox);
		addAnswerBoxChildren.add(this.addAnswerButton);

		Text currentQuestionText = new Text("Current question");
		this.currentQuestionTextArea = new TextArea();
		this.currentQuestionTextArea.setPromptText("Current question text...");
		this.currentQuestionTextArea.setEditable(false);
		Text currentAnswersText = new Text("Answers");
		this.currentAnswersList = new ListView<>();
		ObservableList<Label> currentAnswersListItems = FXCollections.observableArrayList();
		this.currentAnswersList.setItems(currentAnswersListItems);
		this.addCurrentQuestionButton = new Button("Add Question");

		VBox currentQuestionBox = new VBox(5);
		ObservableList<Node> currentQuestionBoxChildren = currentQuestionBox.getChildren();
		currentQuestionBoxChildren.add(currentQuestionText);
		currentQuestionBoxChildren.add(this.currentQuestionTextArea);
		currentQuestionBoxChildren.add(currentAnswersText);
		currentQuestionBoxChildren.add(this.currentAnswersList);
		currentQuestionBoxChildren.add(this.addCurrentQuestionButton);

		Text quizNameText = new Text("Quiz name");
		this.currentQuizNameTextField = new TextField();
		this.currentQuizNameTextField.setEditable(false);
		Text questionsText = new Text("Questions");
		this.questionsList = new ListView<>();
		ObservableList<Label> questionsListItems = FXCollections.observableArrayList();
		this.questionsList.setItems(questionsListItems);
		this.submitQuizButton = new Button("Submit Quiz");

		VBox questionsBox = new VBox(5);
		ObservableList<Node> questionsBoxChildren = questionsBox.getChildren();
		questionsBoxChildren.add(quizNameText);
		questionsBoxChildren.add(currentQuizNameTextField);
		questionsBoxChildren.add(questionsText);
		questionsBoxChildren.add(this.questionsList);
		questionsBoxChildren.add(this.submitQuizButton);

		Separator separator1 = new Separator();
		separator1.setOrientation(Orientation.VERTICAL);
		separator1.setValignment(VPos.CENTER);
		separator1.setPrefHeight(WINDOW_HEIGHT);

		Separator separator2 = new Separator();
		separator2.setOrientation(Orientation.VERTICAL);
		separator2.setValignment(VPos.CENTER);
		separator2.setPrefHeight(WINDOW_HEIGHT);

		HBox makeQuizContentBox = new HBox(5);
		ObservableList<Node> makeQuizContentBoxChildren = makeQuizContentBox.getChildren();
		makeQuizContentBoxChildren.add(addQuestionBox);
		makeQuizContentBoxChildren.add(separator1);
		makeQuizContentBoxChildren.add(currentQuestionBox);
		makeQuizContentBoxChildren.add(separator2);
		makeQuizContentBoxChildren.add(questionsBox);

		this.makeQuizBackButton = new Button("Back");

		VBox makeQuizContainerBox = new VBox(5);
		makeQuizContainerBox.setAlignment(Pos.CENTER);
		makeQuizContainerBox.setPadding(new Insets(5, 5, 5, 5));
		ObservableList<Node> makeQuizContainerBoxChildren = makeQuizContainerBox.getChildren();
		makeQuizContainerBoxChildren.add(makeQuizTopBox);
		makeQuizContainerBoxChildren.add(makeQuizContentBox);
		makeQuizContainerBoxChildren.add(this.makeQuizBackButton);

		this.makeQuizScene = new Scene(makeQuizContainerBox);

		/*
		 * Configuring the quiz taking scene
		 */
		Text takeQuizTitleText = new Text("Take a Quiz");
		takeQuizTitleText.setFont(TITLE_FONT);
		Text selectQuizText = new Text("Select a quiz to take");
		this.selectQuizList = new ListView<>();
		ObservableList<Label> selectQuizListItems = FXCollections.observableArrayList();
		this.selectQuizList.setItems(selectQuizListItems);
		this.startQuizButton = new Button("Start Quiz");
		this.takeQuizBackButton = new Button("Back");

		VBox takeQuizContainer = new VBox(5);
		takeQuizContainer.setPadding(new Insets(5, 5, 5, 5));
		ObservableList<Node> takeQuizContainerChildren = takeQuizContainer.getChildren();
		takeQuizContainerChildren.add(takeQuizTitleText);
		takeQuizContainerChildren.add(selectQuizText);
		takeQuizContainerChildren.add(this.selectQuizList);
		takeQuizContainerChildren.add(this.startQuizButton);
		takeQuizContainerChildren.add(this.takeQuizBackButton);

		this.takeQuizScene = new Scene(takeQuizContainer);

		// navigation event handling
		EventHandler<MouseEvent> makeQuizClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				QuizApplication.this.primaryStage.setScene(QuizApplication.this.makeQuizScene);
				QuizApplication.this.resetQuiz();
			}
		};

		this.makeQuizButton.addEventFilter(MouseEvent.MOUSE_CLICKED, makeQuizClickHandler);

		EventHandler<MouseEvent> makeQuizBackClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				QuizApplication.this.backToMenu();
			}
		};

		this.makeQuizBackButton.addEventFilter(MouseEvent.MOUSE_CLICKED, makeQuizBackClickHandler);

		EventHandler<MouseEvent> takeQuizClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				QuizDataMapper mapper = new QuizDataMapper();
				try {
					Quiz[] quizzes = mapper.getQuizzes();
					QuizApplication.this.quizzes = quizzes;
					ObservableList<Label> quizzesItems = QuizApplication.this.selectQuizList
							.getItems();
					quizzesItems.clear();
					for (Quiz quiz : quizzes) {
						quizzesItems.add(new Label(quiz.getName()));
					}
					QuizApplication.this.primaryStage.setScene(QuizApplication.this.takeQuizScene);
				} catch (IOException ex) {
					ex.printStackTrace();
					QuizApplication.this.showErrorMessage(
							"An error ocurred when attempting to read quiz data.");
				}
			}
		};

		this.takeQuizButton.addEventFilter(MouseEvent.MOUSE_CLICKED, takeQuizClickHandler);

		EventHandler<MouseEvent> takeQuizBackClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				QuizApplication.this.backToMenu();
			}
		};

		this.takeQuizBackButton.addEventFilter(MouseEvent.MOUSE_CLICKED, takeQuizBackClickHandler);

		// quiz maker event handling
		EventHandler<MouseEvent> setQuizNameClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				String name = QuizApplication.this.quizNameTextField.getText();
				if (name.length() > 0) {
					QuizApplication.this.builder.setName(name);
					QuizApplication.this.currentQuizNameTextField.setText(name);
					QuizApplication.this.quizNameTextField.setText("");
				}
			}
		};

		this.setQuizNameButton.addEventFilter(MouseEvent.MOUSE_CLICKED, setQuizNameClickHandler);

		EventHandler<MouseEvent> newQuestionClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				QuizApplication.this.clearCurrentQuestion();
			}
		};

		this.newQuestionButton.addEventFilter(MouseEvent.MOUSE_CLICKED, newQuestionClickHandler);

		EventHandler<MouseEvent> addQuestionTextClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				String text = QuizApplication.this.addQuestionTextArea.getText();
				if (text.length() > 0) {
					QuizApplication.this.currentQuestionTextArea.setText(text);
					QuizApplication.this.addQuestionTextArea.setText("");
				}
			}
		};

		this.addQuestionTextButton.addEventFilter(MouseEvent.MOUSE_CLICKED,
				addQuestionTextClickHandler);

		EventHandler<MouseEvent> addAnswerClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				String text = QuizApplication.this.addAnswerTextArea.getText();
				if (text.length() > 0) {
					ObservableList<Label> itemList = QuizApplication.this.currentAnswersList
							.getItems();
					if (QuizApplication.this.correctAnswerCheckBox.isSelected()) {
						text += CORRECT_ANSWER_INDICATOR;
						QuizApplication.this.correctAnswerCheckBox.setDisable(true);
					}
					itemList.add(new Label(text));
					QuizApplication.this.addAnswerTextArea.setText("");
					QuizApplication.this.correctAnswerCheckBox.setSelected(false);
				}
			}
		};

		this.addAnswerButton.addEventFilter(MouseEvent.MOUSE_CLICKED, addAnswerClickHandler);

		EventHandler<MouseEvent> addCurrentQuestionClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				String questionText = QuizApplication.this.currentQuestionTextArea.getText();
				ObservableList<Label> answerListItems = QuizApplication.this.currentAnswersList
						.getItems();
				int answerCount = answerListItems.size();
				if (questionText.length() > 0 && answerCount > 0) {
					String[] answers = new String[answerCount];
					String currentAnswerText = "";
					int correctIndex = -1;
					for (int i = 0; i < answerCount; i++) {
						currentAnswerText = answerListItems.get(i).getText();
						if (currentAnswerText.endsWith(CORRECT_ANSWER_INDICATOR)) {
							correctIndex = i;
							currentAnswerText = currentAnswerText.substring(0,
									currentAnswerText.lastIndexOf(CORRECT_ANSWER_INDICATOR));
						}
						answers[i] = currentAnswerText;
					}
					QuizApplication.this.builder.addQuestion(questionText, answers, correctIndex);

					QuizApplication.this.clearCurrentQuestion();

					List<Question> quizQuestions = QuizApplication.this.builder.getQuestions();
					QuizApplication.this.questionsList.getItems()
							.add(new Label(quizQuestions.get(quizQuestions.size() - 1).getText()));
				}
			}
		};

		this.addCurrentQuestionButton.addEventFilter(MouseEvent.MOUSE_CLICKED,
				addCurrentQuestionClickHandler);

		EventHandler<MouseEvent> submitQuizClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				String quizName = QuizApplication.this.currentQuizNameTextField.getText();
				if (QuizApplication.this.builder.getQuestions().size() > 0
						&& quizName.length() > 0) {
					QuizApplication.this.buildAndSaveQuiz();
					QuizApplication.this.resetQuiz();
				}
			}
		};

		this.submitQuizButton.addEventFilter(MouseEvent.MOUSE_CLICKED, submitQuizClickHandler);

		// quiz taker event handling
		EventHandler<MouseEvent> startQuizClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				int index = QuizApplication.this.selectQuizList.getSelectionModel()
						.getSelectedIndex();
				Quiz selectedQuiz = QuizApplication.this.quizzes[index];
				QuizApplication.this.prepareQuizScene(selectedQuiz);
				QuizApplication.this.primaryStage.setScene(QuizApplication.this.quizScene);
			}
		};

		this.startQuizButton.addEventFilter(MouseEvent.MOUSE_CLICKED, startQuizClickHandler);

		// setting the primary stage
		this.primaryStage.setWidth(WINDOW_WIDTH);
		this.primaryStage.setHeight(WINDOW_HEIGHT);
		this.primaryStage.setTitle(TITLE);
		this.primaryStage.setScene(welcomeScene);
		this.primaryStage.show();
	}

	private void backToMenu() {
		this.primaryStage.setScene(this.welcomeScene);
	}

	private void buildAndSaveQuiz() {
		Quiz quiz = this.builder.build();
		QuizDataMapper mapper = new QuizDataMapper();
		try {
			mapper.insert(quiz);
			this.showSuccessMessage("Quiz created successfully.");
		} catch (Exception e) {
			e.printStackTrace();
			this.showErrorMessage("Failed to save quiz.");
		}
	}

	private void clearCurrentQuestion() {
		this.addQuestionTextArea.setText("");
		this.addAnswerTextArea.setText("");
		this.correctAnswerCheckBox.setSelected(false);
		this.currentQuestionTextArea.setText("");
		this.currentAnswersList.getItems().clear();
		this.correctAnswerCheckBox.setDisable(false);
	}

	private void prepareQuizScene(Quiz quiz) {
		VBox quizContainer = new VBox(5);
		quizContainer.setPadding(new Insets(5, 5, 5, 5));

		ObservableList<Node> quizContainerChildren = quizContainer.getChildren();

		Text quizNameText = new Text(quiz.getName());
		quizNameText.setFont(TITLE_FONT);
		quizContainerChildren.add(new Text(quiz.getName()));

		HashMap<Integer, Integer> answerMap = new HashMap<>();

		Iterator<Question> quizIterator = quiz.iterator();
		VBox questionBox;
		ObservableList<Node> questionBoxChildren;
		String[] options;
		int index = 0;
		while (quizIterator.hasNext()) {
			Question question = quizIterator.next();
			questionBox = new VBox(5);
			questionBoxChildren = questionBox.getChildren();
			questionBoxChildren.add(new Text(question.getText()));
			options = question.getOptions();
			ToggleGroup group = new ToggleGroup();
			for (int i = 0; i < options.length; i++) {
				RadioButton radioButton = new RadioButton(options[i]);
				radioButton.setUserData(String.format("%d-%d", index, i));
				radioButton.setToggleGroup(group);
				questionBoxChildren.add(radioButton);
			}
			group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
				public void changed(ObservableValue<? extends Toggle> ov, Toggle oldToggle,
						Toggle newToggle) {
					if (group.getSelectedToggle() != null) {
						String userData = group.getSelectedToggle().getUserData().toString();
						Integer questionIndex = Integer.parseInt(userData.split("-")[0]);
						Integer answerIndex = Integer.parseInt(userData.split("-")[1]);
						answerMap.put(questionIndex, answerIndex);
					}
				}
			});
			quizContainerChildren.add(questionBox);
			index++;
		}
		this.finishQuizButton = new Button("Finish");

		EventHandler<MouseEvent> finishQuizClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				Iterator<Question> quizIterator = quiz.iterator();
				Question question;
				for (int i = 0; quizIterator.hasNext(); i++) {
					question = quizIterator.next();
					if (!answerMap.containsKey(i)) {
						QuizApplication.this.showErrorMessage("You must answer all questions.");
						return;
					}
					question.answer(answerMap.get(i));
				}
				QuizApplication.this.prepareResultScene(quiz);
				QuizApplication.this.primaryStage.setScene(QuizApplication.this.resultScene);
			}
		};

		finishQuizButton.addEventFilter(MouseEvent.MOUSE_CLICKED, finishQuizClickHandler);

		this.quizBackButton = new Button("Back");

		EventHandler<MouseEvent> quizBackClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				QuizApplication.this.primaryStage.setScene(QuizApplication.this.welcomeScene);
			}
		};

		this.quizBackButton.addEventFilter(MouseEvent.MOUSE_CLICKED, quizBackClickHandler);

		quizContainerChildren.add(this.finishQuizButton);
		quizContainerChildren.add(this.quizBackButton);

		this.quizScene = new Scene(quizContainer);
	}

	private void prepareResultScene(Quiz quiz) {
		VBox resultContainer = new VBox(5);
		resultContainer.setPadding(new Insets(5, 5, 5, 5));
		resultContainer.setAlignment(Pos.CENTER);

		ObservableList<Node> resultContainerChildren = resultContainer.getChildren();

		Text resultTitle = new Text("Results");
		resultTitle.setFont(TITLE_FONT);
		resultContainerChildren.add(resultTitle);

		resultContainerChildren.add(new Separator(Orientation.HORIZONTAL));

		Iterator<Question> quizIterator = quiz.iterator();
		Question question;
		ObservableList<Node> questionContainerChildren;
		int i;
		int correctCount = 0;
		for (i = 0; quizIterator.hasNext(); i++) {
			question = quizIterator.next();
			VBox questionContainer = new VBox(5);
			questionContainer.setAlignment(Pos.CENTER);
			questionContainer.setPadding(new Insets(5, 5, 5, 5));
			questionContainerChildren = questionContainer.getChildren();
			questionContainerChildren.add(new Text(question.getText()));
			questionContainerChildren.add(
					new Text("You answered: " + question.getOptions()[question.getAnswerIndex()]));
			Text correctText = new Text();
			if (question.isCorrect()) {
				correctText.setText("Correct!");
				correctText.setFill(Color.GREEN);
				correctCount++;
			} else {
				correctText.setText("Incorrect");
				correctText.setFill(Color.RED);
			}
			questionContainerChildren.add(correctText);
			resultContainerChildren.add(questionContainer);
			resultContainerChildren.add(new Separator(Orientation.HORIZONTAL));
		}

		Text scoreText = new Text("Your score: " + correctCount + " out of " + i);
		scoreText.setFont(new Font(28));
		resultContainerChildren.add(scoreText);

		this.resultBackButton = new Button("Back to menu");

		EventHandler<MouseEvent> resultBackClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				QuizApplication.this.primaryStage.setScene(QuizApplication.this.welcomeScene);
			}
		};

		this.resultBackButton.addEventFilter(MouseEvent.MOUSE_CLICKED, resultBackClickHandler);

		resultContainerChildren.add(this.resultBackButton);

		this.resultScene = new Scene(resultContainer);
	}

	private void resetQuiz() {
		this.clearCurrentQuestion();
		this.builder = new QuizBuilder();
		this.questionsList.getItems().clear();
		this.currentQuizNameTextField.setText("");
		this.quizNameTextField.setText("");
	}

	private void showErrorMessage(String messageBody) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(null);
		alert.setContentText(messageBody);
		alert.showAndWait();
	}

	private void showSuccessMessage(String messageBody) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Success");
		alert.setHeaderText(null);
		alert.setContentText(messageBody);
		alert.showAndWait();
	}

	public static void main(String[] args) {
		QuizApplication.launch(args);
	}
}

package oodp.assignment2.quiz.io;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.stream.JsonWriter;

import oodp.assignment2.quiz.Quiz;

public class QuizDataMapper implements QuizDataMapperInterface {

	private static final String DATA_LOCATION = "./data/";

	public void insert(Quiz quiz) throws JsonIOException, IOException {
		Gson gson = new Gson();
		String name = QuizIOUtil.generateQuizFileName(quiz);
		String fileName = DATA_LOCATION + name;
		String jsonString = gson.toJson(quiz);
		JsonWriter writer = new JsonWriter(new FileWriter(fileName));
		writer.jsonValue(jsonString);
		writer.close();
	}

	public Quiz[] getQuizzes() throws IOException {
		Gson gson = new Gson();
		File dir = new File(DATA_LOCATION);
		File[] files = dir.listFiles();
		java.util.List<Quiz> quizzes = new java.util.ArrayList<>();
		FileReader reader = null;
		String currStr = "";
		int currChar;
		for (int i = 0; i < files.length; i++) {
			currStr = "";
			if (QuizIOUtil.isDataFile(files[i])) {
				reader = new FileReader(files[i]);
				currChar = reader.read();
				while (currChar != -1) {
					currStr += (char) currChar;
					currChar = reader.read();
				}
				quizzes.add(gson.fromJson(currStr, Quiz.class));
			}
		}
		return quizzes.toArray(new Quiz[quizzes.size()]);
	}

}

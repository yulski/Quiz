package oodp.assignment2.quiz.io;

import oodp.assignment2.quiz.Quiz;

public interface QuizDataMapperInterface {

	public void insert(Quiz quiz) throws Exception;

	public Quiz[] getQuizzes() throws Exception;

}

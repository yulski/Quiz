package oodp.assignment2.quiz.io;

import java.io.File;

import oodp.assignment2.quiz.Quiz;

public class QuizIOUtil {

	public static String generateQuizFileName(Quiz quiz) {
		String name = quiz.getName();
		return name.replaceAll("[^A-Za-z0-9]", "") + ".json";
	}

	public static boolean isDataFile(File file) {
		String name = file.getName();
		return name.charAt(0) != '.' && name.endsWith(".json");
	}

}
